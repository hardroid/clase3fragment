package cl.citymovil.ejemplofragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.gson.Gson;

import cl.citymovil.ejemplofragments.entidades.Persona;

public class ActivitySegunda extends AppCompatActivity {

    public static final String KEY_PARAM = "key_param";
    public static final String KEY_PARAM_JSON = "key_parma_json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activityegunda);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Persona param = getIntent().getParcelableExtra(KEY_PARAM);
        /*String paramJson = getIntent().getStringExtra(KEY_PARAM_JSON);
        Gson gson = new Gson();*/
        Persona p2 = Persona.obtienePersona(getIntent().getStringExtra(KEY_PARAM_JSON));//)gson.fromJson(paramJson, Persona.class);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

}
