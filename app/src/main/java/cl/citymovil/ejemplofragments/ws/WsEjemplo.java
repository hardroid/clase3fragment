package cl.citymovil.ejemplofragments.ws;

import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import cl.citymovil.ejemplofragments.entidades.Persona;
import cl.citymovil.ejemplofragments.entidades.Personas;

/**
 * Created by harce on 05-10-2015.
 */
public class WsEjemplo {

    private static final long TIME_OUT_SECONDS = 5000;
    private static WsEjemplo instance;

    public static WsEjemplo getInstance() {
        if (instance == null) {
            instance = new WsEjemplo();
        }
        return instance;
    }

    public Persona[] listadoPersonas(){
        Persona[] retorno = null;

        String url = "https://script.googleusercontent.com/macros/echo?user_content_key=ijRoMos-qBxz-Hu6cCaZ00wUpH89trE8rG440ijRJULMAD7Pl0zG2rw48ifiSQertqustk3vXxxmN-WwwuMcZDNkwGpNdibOm5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnA0thbgVnNY_b9pWgKbaJaP2RmjVkmUJsutrxCpmKRaWxyds-PGOsPBn-Y9wePuUS_cCtl-j_Nje&lib=MwpmAQL5DdOdF-w2pxAsO6wRCOmyOBSQj";
        try {
            String respuesta = getRequest(url);


            final Gson gson = new Gson();
            retorno = gson.fromJson(respuesta, Persona[].class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retorno;
    }

    /*public RespuestaPost registroPersona(Context context, Persona values) {
        RespuestaPost retorno = null;
        FormEncodingBuilder formBody = new FormEncodingBuilder()
                .add("id", values.getId())
                .add("nombre", values.getNombre())
                .add("apellido", values.getApellido())
                .add("rut", values.getRut());

        String url = context.getResources().getString(R.string.url_servicio);
        try {
            String respuesta = postRequest(url, formBody);

            final GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(RespuestaPost.class, new AgregarPersonaGSON());
            final Gson gson = gsonBuilder.create();
            retorno = gson.fromJson(respuesta, RespuestaPost.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retorno;
    }

    public RespuestaGet listarPersonas(Context context) {
        RespuestaGet retorno = null;

        String url = context.getResources().getString(R.string.url_servicio);
        try {
            String respuesta = getRequest(url);

            final GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(RespuestaGet.class, new ListarPersonaGSON());
            final Gson gson = gsonBuilder.create();
            retorno = gson.fromJson(respuesta, RespuestaGet.class);
        } catch (Exception e) {

        }
        return retorno;
    }*/

    private String postRequest(String url, FormEncodingBuilder params) throws IOException {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS);

        RequestBody formBody = params.build();


        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String respuesta = response.body().string();
        return respuesta;
    }

    private String getRequest(String url)  throws IOException {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS);
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String respuesta = response.body().string();
        return respuesta;
    }

}
