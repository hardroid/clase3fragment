package cl.citymovil.ejemplofragments;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;

import cl.citymovil.ejemplofragments.entidades.Persona;
import cl.citymovil.ejemplofragments.entidades.Personas;
import cl.citymovil.ejemplofragments.ws.WsEjemplo;

public class MainActivity extends AppCompatActivity {

    BlankFragment blankFragment;
    Button btnCambiarVentana;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCambiarVentana = (Button)findViewById(R.id.btnCambiarVentana);
        btnCambiarVentana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarVentana();
            }
        });

/*

        blankFragment = BlankFragment.newInstance("Parametro uno", "Parametro dos");
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.contentPanel, blankFragment, "blanckFragment");
        ft.commit();*/


    }

    private void cambiarVentana() {

        Persona p = new Persona();
        p.setApellido("Arce");
        p.setId("1");
        p.setRut("1-9");
        p.setNombre("Harttyn");
        Intent i = new Intent(this, ActivitySegunda.class);
        i.putExtra(ActivitySegunda.KEY_PARAM, p);

        /*
        Gson gson = new Gson();
        String json = gson.toJson(p);*/
        i.putExtra(ActivitySegunda.KEY_PARAM_JSON, p.toString());
        startActivity(i);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new EjecutaServicio(this).execute();
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    class EjecutaServicio extends AsyncTask<Void, Void, Persona[]>{

        private final Context context;
        private ProgressDialog progressDialog;

        public EjecutaServicio(Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                progressDialog = new ProgressDialog(context);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage(context.getResources().getString(R.string.cargando_datos));
                progressDialog.setCancelable(false);
                progressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Persona[] doInBackground(Void... params) {

            Persona[] respuesta;
            try{
                respuesta = WsEjemplo.getInstance().listadoPersonas();
                return respuesta;
            }catch (Exception e){

            }

            return null;
        }

        @Override
        protected void onPostExecute(Persona[] personas) {
            super.onPostExecute(personas);
            cargarDatos(personas);
            progressDialog.hide();

        }
    }

    private void cargarDatos(Persona[] personas) {
        if(personas != null) {
            Toast.makeText(this, "Existen " + personas.length + " personas", Toast.LENGTH_LONG).show();
        }
    }
}
