package cl.citymovil.ejemplofragments;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import cl.citymovil.ejemplofragments.entidades.Persona;

/**
 * Created by salemlabs on 12-05-16.
 */
public class SesionPreferences {

    private static final String KEY_SHARED = "key_sesion";
    private static final String KEY_PERSONA = "key_nombre";
    private static SesionPreferences instance;

    private SesionPreferences(){}

    public static SesionPreferences getIntance(){
        if(instance == null){
            instance = new SesionPreferences();
        }
        return instance;
    }

    public void guardarPersona(Context context, Persona persona){
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(KEY_PERSONA, persona.toString());
        edit.commit();
    }

    public Persona obtienePersona(Context context){
        SharedPreferences  sharedPreferences = context.getSharedPreferences(KEY_SHARED, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString(KEY_PERSONA, "");
        if(value.length() > 0){
            Gson gson = new Gson();
            Persona p = gson.fromJson(value, Persona.class);
            return p;
        }else{
            return null;
        }
    }

    public void borrarPersona(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(KEY_PERSONA, "");
        edit.commit();
    }

}
