package cl.citymovil.ejemplofragments.entidades;

import java.util.ArrayList;

/**
 * Created by salemlabs on 04-05-16.
 */
public class Personas {
    ArrayList<Persona> personas;

    public ArrayList<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ArrayList<Persona> personas) {
        this.personas = personas;
    }
}
